package cadastroalunodisciplina;

import java.util.Scanner;
import java.io.*;

public class CadastroAlunoDisciplina{

	public static void main(String [] args) throws IOException{
	//burocracia
	InputStream entradaSistema = System.in;
	InputStreamReader leitor = new InputStreamReader(entradaSistema);
	BufferedReader leitorEntrada = new BufferedReader(leitor);
	String entradaTeclado;

	//instaciando
	ControleAluno umControleAluno = new ControleAluno();
	ControleDisciplina umControleDisciplina = new ControleDisciplina();
	Aluno umAluno = new Aluno();
	Disciplina umaDisciplina = new Disciplina();

	//variavel menu
	int opcao;
	Scanner entrada = new Scanner(System.in);

	//menu
	
		do{

			System.out.println("\tMenu");
			System.out.println("1 - Adicionar Disciplina");
			System.out.println("2 - Adicionar Aluno");
			System.out.println("3 - Remover Disciplina");
			System.out.println("4 - Remover Aluno");
			System.out.println("5 - Pesquisar Disciplina");
			System.out.println("6 - Pesquisar Aluno");
			System.out.println("7 - Sair");

			System.out.println("Escolha uma opção");
			opcao = entrada.nextInt();
		
			switch(opcao){
							//Adicionar disciplina
						case 1:
                                System.out.println("Digite o nome da disciplina: ");
								entradaTeclado = leitorEntrada.readLine();
								umaDisciplina.setDisciplina(entradaTeclado);
						
								System.out.println("Digite o nome do professor da disciplina: ");
								entradaTeclado = leitorEntrada.readLine();
								umaDisciplina.setProfessor(entradaTeclado);
					
								String testeSaida = umControleDisciplina.adicionar(umaDisciplina);

								//conferindo saída
								System.out.println("**************************");
								System.out.println(testeSaida);
								System.out.println("**************************");
						
								break;
						//Adicionar aluno
						case 2:
								System.out.println("Digite o nome do aluno: ");
								entradaTeclado = leitorEntrada.readLine();
								umAluno.setNome(entradaTeclado);
					
								System.out.println("Digite a matricula do aluno: ");
								entradaTeclado = leitorEntrada.readLine();
								umAluno.setMatricula(entradaTeclado);
						
								testeSaida = umControleAluno.adicionar(umAluno);

								//conferindo saída
								System.out.println("**************************");
								System.out.println(testeSaida);
								System.out.println("**************************");
						
								break;
						//Remover disciplina
						case 3:
								System.out.println("Digite o nome da disciplina: ");
								entradaTeclado = leitorEntrada.readLine();
								umaDisciplina = umControleDisciplina.pesquisar(entradaTeclado);
								umControleDisciplina.remover(umaDisciplina);
						
								break;
						//Remover aluno
						case 4:
								System.out.println("Digite o nome do aluno: ");
								entradaTeclado = leitorEntrada.readLine();
								umAluno = umControleAluno.pesquisar(entradaTeclado);
								umControleAluno.remover(umAluno);
						
								break;
						// Pesquisar disciplina
						case 5: 
                                System.out.println("Digite o nome da disciplina: ");
                                entradaTeclado = leitorEntrada.readLine();
                                umControleDisciplina.pesquisar(entradaTeclado);
                                System.out.println("Disciplina: " +umaDisciplina.getDisciplina());
                                System.out.println("Professor (a): " +umaDisciplina.getProfessor());
                    
                                break;
						// Pesquisar aluno
						case 6: 
                                System.out.println("Digite o nome do aluno: ");
                                entradaTeclado = leitorEntrada.readLine();
                                umControleAluno.pesquisar(entradaTeclado);
                                System.out.println("Nome: " + umAluno.getNome());
                                System.out.println("Matricula: " + umAluno.getMatricula());
                    
                                break;
                                 
                        // Saindo do programa
                        case 7:
                                opcao = 7;
                                break;
						default :
								System.out.println("Opção inválida!");

			}
		}while( opcao != 7);
		
    }

}
