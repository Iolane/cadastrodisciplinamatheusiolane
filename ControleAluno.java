package cadastroalunodisciplina;
import java.util.*;

public class ControleAluno{
	//atributos

	private ArrayList<Aluno> listaAlunos;

	//construtor
	
	public ControleAluno(){
		listaAlunos = new ArrayList<Aluno>();
	}

	//métodos

	public String adicionar(Aluno umAluno){
		String mensagem = "Aluno matriculado"; 
		listaAlunos.add(umAluno);
		return mensagem;
	}

	public String remover(Aluno umAluno){
		String mensagem = "Aluno jubilado";
		listaAlunos.remove(umAluno);
		return mensagem;
	}

	public Aluno pesquisar(String umAluno){
		for( Aluno alunoPesquisado : listaAlunos ){
			if( alunoPesquisado.getNome().equalsIgnoreCase(umAluno) )
				return alunoPesquisado;
		}
            return null;
	
    }
}

