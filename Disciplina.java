package cadastroalunodisciplina;
import java.util.ArrayList;

public class Disciplina{

	private String nomeDisciplina;
	private String professorDisciplina;
	private ArrayList<Aluno> listaAluno;


//construtor

		public Disciplina(){
			listaAluno = new ArrayList<Aluno>();
		}

//Métodos

	public void setDisciplina(String umaDisciplina){
		nomeDisciplina = umaDisciplina;	
	}
	
	public String getDisciplina(){
		return nomeDisciplina;
	}

	public void setProfessor(String umProfessor){
		professorDisciplina = umProfessor;
	}

	public String getProfessor(){
		return professorDisciplina;
	}	

}