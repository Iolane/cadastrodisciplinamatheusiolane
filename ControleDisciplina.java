package cadastroalunodisciplina;
import java.util.ArrayList;

public class ControleDisciplina{

	//atributos

	private ArrayList<Disciplina> listaDisciplinas;

	//construtor
	
	public ControleDisciplina(){
		listaDisciplinas = new ArrayList<Disciplina>();
	}

	//métodos

	public String adicionar(Disciplina umaDisciplina){
		String mensagem = "Disciplina adicionada"; 
		listaDisciplinas.add(umaDisciplina);
		return mensagem;
	}

	public String remover(Disciplina umaDisciplina){
		String mensagem = "Disciplina removida"; 
		listaDisciplinas.remove(umaDisciplina);
		return mensagem;
	}


	public Disciplina pesquisar(String umaDisciplina){
		for(Disciplina disciplinaPesquisada : listaDisciplinas){
			if(disciplinaPesquisada.getDisciplina().equalsIgnoreCase(umaDisciplina))
				return disciplinaPesquisada;
			}
		return null;	
	
    }

}